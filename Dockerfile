FROM alpine

RUN apk --update add --no-cache openjdk17-jdk curl jq tzdata

WORKDIR /opt/app

ENTRYPOINT ["/bin/sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -XshowSettings:vm ${JAVA_OPTS} ${PINPOINT_OPTS} -jar /opt/app/app.jar \"$@\""]
